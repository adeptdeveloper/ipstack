
$(document).ready(function()
{
	$('#btnIPLookup').click(function()
	{
		var IPAddress = $('#IPAddress').val();
		var Language = $('#Language').val();
		var Hostname = $('#Hostname').prop('checked');
		var Security = $('#Security').prop('checked');
		var Fields = $('#Fields').val();
		var Output = $('#Output').val();
		var Callback = $('#Callback').val();

		$('#divResults').load(
			'_ajax/IPLookup.cfm',
			{
				'IPAddress': IPAddress,
				'Language': Language,
				'Hostname': Hostname,
				'Security': Security,
				'Fields': Fields,
				'Output': Output,
				'Callback': Callback
			},
			function()
			{

			}
		);
	});
});
