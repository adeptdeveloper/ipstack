<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="https://static1.adeptdeveloper.net/favicon.ico">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="http://www.adeptdeveloperlabs.com/_assets/css/adeptdeveloperlabs.css">

    <title>IP Stack CFC</title>
  </head>
  <body>


	<div class="container-fluid">
		<nav class="navbar navbar-expand navbar-dark bg-dark flex-column flex-md-row fixed-top">
			<a class="navbar-brand" href="/">IPStack.cfc</a>
		</nav>

		<div class="row flex-xl-nowrap" style="top: 56px; position: relative;">
			<cfinclude template="sidebar-left.cfm">

			<main class="col-md-9 col-xl-8 py-md-3 pl-md-5" role="main">
