<div id="sidebarleft" class="col-md-4 col-xl-3 pt-3">
	<ul class="nav flex-column flex-nowrap overflow-hidden pt-2">
		<li class="nav-item">
			<a class="nav-link text-truncate" href="#"><i class="fa fa-home"></i> <span class="d-none d-sm-inline"><strong>Getting Started</strong></span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link text-truncate" href="/demo/IPLookup.cfm"><span class="d-none d-sm-inline"><strong>IP Lookup</strong></span></a>
		</li>
	</ul>
</div>
