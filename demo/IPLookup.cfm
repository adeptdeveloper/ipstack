
<cfoutput>
	<div class="row">
		<h1>IP Lookup</h1>

		<p>Pass any IPv4 or IPv6 address to do a lookup.</p>
	</div>

	<div class="row">
		<h2>Parameters</h2>
	</div>

	<div class="row">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Parameter</th>
					<th>Type</th>
					<th>Required</th>
					<th>Default</th>
					<th>Value</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>IP Address</td>
					<td>string</td>
					<td>required</td>
					<td></td>
					<td><input type="text" id="IPAddress" name="IPAddress" value="" placeholder="IP Address" class="form-control"></td>
				</tr>
				<tr>
					<td>Hostname</td>
					<td>bit</td>
					<td>optional</td>
					<td>0</td>
					<td><input type="checkbox" id="Hostname" name="Hostname" value="1" class="form-control"></td>
				</tr>
				<tr>
					<td>Security</td>
					<td>bit</td>
					<td>optional</td>
					<td>0</td>
					<td><input type="checkbox" id="Security" name="Security" value="1" class="form-control"></td>
				</tr>
				<tr>
					<td>Language</td>
					<td>string</td>
					<td>optional</td>
					<td>en</td>
					<td><input type="text" id="Language" name="Language" value="" placeholder="2 or 3 digit language code such as EN or ES" class="form-control"></td>
				</tr>
				<tr>
					<td>Fields</td>
					<td>string</td>
					<td>optional</td>
					<td></td>
					<td><input type="text" id="Fields" name="Fields" value="" placeholder="" class="form-control"></td>
				</tr>
				<tr>
					<td>Output</td>
					<td>string</td>
					<td>optional</td>
					<td></td>
					<td><input type="text" id="Output" name="Output" value="" placeholder="" class="form-control"></td>
				</tr>
				<tr>
					<td>Callback</td>
					<td>string</td>
					<td>optional</td>
					<td></td>
					<td><input type="text" id="Callback" name="Callback" value="" placeholder="" class="form-control"></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="row">
		<button id="btnIPLookup" class="btn btn-primary" type="submit">Try <i class="fas fa-arrow-right"></i></button>
	</div>

	<div class="row mt-4">
		<div id="divResults"></div>
	</div>
</cfoutput>

