
<cfparam name="Request.Attributes.AccessKey" default="#Application.Settings.AccessKey#">

<cfset IPStack = CreateObject('Component', 'lib.IPStack.IPStack')>
<cfset IPStack.SetAccessKey(Request.Attributes.AccessKey)>

<cfset stIPLookup = IPStack.IPLookup(
	IPAddress = Request.Attributes.IPAddress,
	Language = Request.Attributes.Language,
	Hostname = Request.Attributes.Hostname,
	Security = Request.Attributes.Security,
	Fields = Request.Attributes.Fields,
	Output = Request.Attributes.Output,
	Callback = Request.Attributes.Callback
)>

<h2>Code</h2>

<h2>Response</h2>

<div class="row pt-2" style="margin: 0 2px;">
	<cfdump var="#stIPLookup#">
</div>
