
<cfcomponent displayname="IPStack">
	<cfset This.Namespace = 'adept.ipstack'>
	<cfset This.AccountType = 'Basic'>
	<cfset This.Protocol = 'http'>
	<cfset This.BaseEndpoint = '#This.Protocol#://api.ipstack.com'>

	<cfset This.Credentials = StructNew()>
	<cfset This.Credentials.AccessKey = ''>

	<cfset This.Version = StructNew()>
	<cfset This.Version.Major = 1>
	<cfset This.Version.Minor = 0>
	<cfset This.Version.Patch = 0>

	<cffunction name="SetAccessKey" access="public" output="false" returntype="void">
		<cfargument name="AccessKey" type="string" required="true">

		<cfset This.Credentials.AccessKey = Trim(Arguments.AccessKey)>
	</cffunction>

	<cffunction name="Dump" access="public" output="false" returntype="struct">
		<cfreturn This>
	</cffunction>

	<cffunction name="IPLookup" access="public" output="false" returntype="struct">
		<cfargument name="IPAddress" type="string" required="true">
		<cfargument name="Language" type="string" required="false" default="en" hint="2 or 3 digit code representing what language to return the response in.">
		<cfargument name="Hostname" type="boolean" required="false" default="0">
		<cfargument name="Security" type="boolean" required="false" default="0">
		<cfargument name="Fields" type="string" required="false" default="">
		<cfargument name="Output" type="string" required="false" default="json" hint="Currently only support json. XML and geojson are the other two options.">
		<cfargument name="Callback" type="string" required="false" default="">

		<cfset var EndPoint = This.BaseEndpoint & '/'>
		<cfset var stResponse = StructNew()>

		<cfset stResponse.Error = 0>
		<cfset stResponse.Response = StructNew()>

		<cfif NOT Len(This.Credentials.AccessKey)>
			<cfthrow type="#This.Namespace#" errorcode="MissingAccessKey" message="AccessKey is missing" detail="You must initialize the CFC using the SetAccessKey method to set your PositionTrack Access Key">
		</cfif>

		<!--- Base Endpoint --->
		<cfset EndPoint = EndPoint & '#Arguments.IPAddress#?access_key=#This.Credentials.AccessKey#'>

		<!--- These features are for paid accounts --->
		<cfif This.AccountType IS NOT 'Free'>
			<cfif Len(Arguments.Language)>
				<cfset EndPoint = EndPoint & '&language=#Arguments.Language#'>
			</cfif>
		</cfif>

		<cfif Len(Arguments.Hostname) AND Arguments.Hostname IS 1>
			<cfset EndPoint = EndPoint & '&hostname=1'>
		</cfif>

		<cfif Len(Arguments.Security) AND Arguments.Security IS 1>
			<cfset EndPoint = EndPoint & '&security=1'>
		</cfif>

		<cfif Len(Arguments.Fields)>
			<cfset EndPoint = EndPoint & '&fields=#Arguments.Fields#'>
		</cfif>

		<cfif Len(Arguments.Output)>
			<cfset EndPoint = EndPoint & '&output=#Arguments.Output#'>
		</cfif>

		<cfif Len(Arguments.Callback)>
			<cfset EndPoint = EndPoint & '&callback=#Arguments.Callback#'>
		</cfif>

		<cflog file="#This.Namespace#" type="information" text="#GetFunctionCalledName()#:#EndPoint#">

		<!--- Call API --->
		<cftry>
			<cfhttp url="#EndPoint#" method="GET">
				<cfhttpparam type="header" name="Content-Type" value="application/json; charset=utf-8">
			</cfhttp>

			<cfset stResponse.Response = DeserializeJson(cfhttp.FileContent)>

			<cfif StructKeyExists(stResponse.Response, 'Error')>
				<cfset stResponse.Error = 1>
			</cfif>

			<cfcatch type="any">
				<cfset stResponse.Error = 1>
				<cflog file="#This.Namespace#" type="error" text="#GetFunctionCalledName()#|#cfhttp.FileContent#">
				<cflog file="#This.Namespace#" type="error" text="#GetFunctionCalledName()#|#SerializeJson(cfhttp)#">
			</cfcatch>
		</cftry>

		<cfreturn stResponse>
	</cffunction>
</cfcomponent>
